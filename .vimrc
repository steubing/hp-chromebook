" ~/.vimrc
"         _
"  __   _(_)_ __ ___  _ __ ___
"  \ \ / / | '_ ` _ \| '__/ __|
"   \ V /| | | | | | | | | (__
"  (_)_/ |_|_| |_| |_|_|  \___|
"
" Scott Steubing

" Bringing in defaults.vim  ...
unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

" Autoloading plug.vim if it's not installed ...

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Registering vimwiki in ~/Dropbox/vimwiki
" put Vimwiki in my MEGA folder; autogenerate the diary index
let g:vimwiki_list = [{'path': '~/Dropbox/vimwiki/', 'auto_diary_index': 1}]

" Plugins, vim-plug required
call plug#begin('~/.vim/plugged')
" Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'vimwiki/vimwiki' " vimwiki
Plug 'csexton/trailertrash.vim'
Plug 'mattn/calendar-vim'
Plug 'ap/vim-css-color'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Yggdroot/indentLine'
Plug 'vifm/vifm.vim'
call plug#end()

" Required for vim-plug
set nocompatible
filetype plugin on
syntax on

" Solarized theme ...
set background=dark
" colorscheme solarized
" colorscheme dracula

set number
set shiftwidth=4
set tabstop=4 softtabstop=4
set expandtab
set smartindent
set nowrap
set ignorecase
set smartcase
set incsearch
"set listchars=trail:-

set colorcolumn=80
highlight ColorColumn ctermbg=17 guibg=lightgrey

"set cursorcolumn
"set cursorline

set splitbelow splitright

let g:airline_theme='simple'
