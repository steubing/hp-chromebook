#!/bin/bash
rofi -modi drun -show drun -line-padding 4 \
                -columns 2 -padding 50 -hide-scrollbar  \
                -disable-history -sort \
                -show-icons -drun-icon-theme "Numix-GalliumOS" -font "MesloLGS NF Regular 10"
