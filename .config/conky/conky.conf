conky.config = {
    alignment = 'top_right',
    background = true,
    border_width = 1,
    cpu_avg_samples = 2,
    default_color = 'white',
    default_outline_color = 'white',
    override_utf8_locale = true,
    default_shade_color = 'white',
    draw_borders = false,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    xftalpha = 1.0,
    total_run_times = 0,
    gap_x = 20,
    gap_y = 55,
    minimum_height = 500,
    minimum_width =240,
    maximum_width = 260,
    net_avg_samples = 2,
    own_window = true,
    own_window_transparent = false,
    own_window_type = 'dock',
    own_window_class = 'Conky',
    own_window_argb_visual = true,
    own_window_argb_value = 128,
    own_window_colour = '#000',
    own_window_hints = 'below,sticky,skip_taskbar,undecorated,skip_pager',
    double_buffer = true,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    stippled_borders = 0,
    update_interval = 1.0,
    uppercase = true,
    use_spacer = 'none',
    show_graph_scale = false,
    show_graph_range = false,
    font = 'Prime',
}

conky.text = [[
${color grey}$alignc${font Prime:weight=bold:size=22}${time %l:%M %p}
${font Prime}${color grey}$alignc${time %d %B %Y}

${color grey}Distro $color$alignr${exec lsb_release -ds | sed 's/"//g'}
${color grey}Kernel $color$alignr$kernel
${color grey}Uptime:$color $alignr$uptime

${color grey}CPU $color$alignr${execi 1000 cat /proc/cpuinfo|grep 'model name'|sed -e's/model name.*: //'|uniq|cut -c 1-25}
${color grey}Frequency $color$alignr$freq_g Ghz
${color grey}Core 0 $color$alignc${cpu cpu0}%$alignr${execi 5 sensors|grep 'Core 0'| awk -F'+' '{print $2}' | awk -F'.' '{print $1}'} °C
${color grey}Core 1 $color$alignc${cpu cpu1}%$alignr${execi 5 sensors|grep 'Core 1'| awk -F'+' '{print $2}' | awk -F'.' '{print $1}'} °C

${color grey}Res $color$alignr${exec xdpyinfo | awk '/dimensions/{print $2}'}

${color grey}${exec xset q | grep Caps | cut -c 9-18}$color$alignr${exec xset q | grep Caps | cut -c 22-24}

${color grey}RAM $color$alignr$mem/$memmax
${color #BEC8CB}${membar 4}
${color grey}Swap $color$alignr$swap/$swapmax
${color #BEC8CB}${swapbar 4}
#${color grey}CPU $color$alignr$cpu%
#${color #BEC8CB}${cpubar 4}

Root $color$alignr${fs_used /}/${fs_size /}
${color #BEC8CB}${fs_bar 4 /}
#Home $color$alignr${fs_used /home}/${fs_size /home}
#${color #BEC8CB}${fs_bar 4 /home}

${color grey}Battery $alignr${battery_percent BAT0}%
${color #BEC8CB}${battery_bar 4 BAT0}
${if_existing /sys/class/net/eth0/operstate up}

{color grey}Up $color$alignr${upspeedf eth0}
${color grey}${upspeedgraph eth0 25 #BEC8CB}
${color grey}Down $color$alignr${downspeedf eth0}
${color grey}${downspeedgraph eth0 25 #BEC8CB}
${endif}${if_existing /sys/class/net/wlan0/operstate up}

{color grey}Up $color$alignr${upspeedf wlan0}
${color grey}${upspeedgraph wlan0 25 #BEC8CB}
${color grey}Down $color$alignr${downspeedf wlan0}
${color grey}${downspeedgraph wlan0 25 #BEC8CB}
${endif}${if_up usb0}

${color grey}Up $color$alignr${upspeedf usb0}
${color grey}${upspeedgraph usb0 25 #BEC8CB}
${color grey}Down $color$alignr${downspeedf usb0}
${color grey}${downspeedgraph usb0 25 #BEC8CB}
${endif}${if_existing /proc/net/route wlp1s0}
${color grey}Up $color$alignr${upspeedf wlp1s0}
${color grey}${upspeedgraph wlp1s0 25 #BEC8CB}
${color grey}Down $color$alignr${downspeedf wlp1s0}
${color grey}${downspeedgraph wlp1s0 25 #BEC8CB}
${endif}
#${color grey}Processes ${alignr}PID
#$color${top name 1} $alignr${top pid 1}
#$color${top name 2} $alignr${top pid 2}
#$color${top name 3} $alignr${top pid 3}
#$color${top name 4} $alignr${top pid 4}
]]
